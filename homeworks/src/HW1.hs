module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a+b 

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 1 = 1.0
hw1_2 n | n < 1 = 0
	| otherwise  = 1.0/fromIntegral n ^ fromIntegral n + hw1_2(n-1)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n <= 0 = 1   
        | otherwise = n * fact2(n-2)  

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime p | p > 1    =  checkPrime 2 p
	  | otherwise = False

checkPrime :: Integer -> Integer -> Bool
checkPrime a p  | fromIntegral a  > sqrt(fromIntegral p)  = True   
                | p `mod` a == 0 && p /= a  = False  
                | otherwise       = checkPrime (a+1) p 

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b | a > b      = 0
	     | b < 2      = 0
             | isPrime a  = a + primeSum (a+1) b
	     | otherwise  = primeSum (a+1) b 
