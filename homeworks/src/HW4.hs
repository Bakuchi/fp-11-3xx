-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String
  
data A = A Int
       | B

instance Show' A where
      show' B                 = "B" 
      show' (A x) | x>=0      = "A " ++ show x
                  | otherwise = "A (" ++ show x ++ ")"
 
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' C where
      show' (C x y) | x>=0 && y>=0 = "C " ++ show x ++ " " ++ show y 
                    | x>=0 && y<=0 = "C " ++ show x ++ " (" ++ show y ++ ")" 
                    | x<=0 && y<=0 = "C (" ++ show x ++ ") (" ++ show y ++ ")"  
                    | otherwise    = "C (" ++ show x ++ ") " ++ show y   


data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set a) (Set b) = Set $ \x -> a x /= b x
-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
tru = \t -> (\f -> t)
fls = \t f -> f
fromBool True = tru
fromBool False = fls

-- fromInt - переводит число в кодировку Чёрча
fromInt 0 = \s z -> z
fromInt n = \s z -> s (fromInt (n-1) s z)
