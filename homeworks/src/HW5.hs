-- 2016-04-05 / 2016-04-12

module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

import Control.Applicative hiding((<|>),many)
{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char " ++ [c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

{--========  END PARSER ========-}

-- ((())())
dyckLanguage :: Parser String
dyckLanguage = (many $ (left <|> right))
left = matching (\c -> c=='(' )
right = matching (\c -> c==')' )
lr = (\l r -> "()")<$> left <*>  right

data Arith = Plus Arith Arith
           | Minus Arith Arith
           | Mul Arith Arith
           | Number Int
           deriving (Eq,Show)

-- ((123+4321)*(321-3123))+(123+321)
{- Add (Mul (Add (Const 123) (Const 4321))
            (Sub (Const 321) (Const 3213)))
       (Add (Const 123) (Const 321))
-}
arith :: Parser Arith
arith = mul <|> (plus <|> (minus <|> num))

num = Number <$> number
mul = (\_ a _ b _ -> Mul a b)
      <$> char '('
      <*> arith
      <*> char '*'
      <*> arith
      <*> char ')'
plus = (\_ a _ b _ -> Plus a b)
      <$> char '('
      <*> arith
      <*> char '+'
      <*> arith
      <*> char ')'
minus = (\_ a _ b _ -> Minus a b)
      <$> char '('
      <*> arith
      <*> char '-'
      <*> arith
      <*> char ')'

-- Инстансы функтора и аппликативного функтора

data Optional a = NoParam
                | Param a
                deriving (Eq,Show)

instance Functor Optional where
  fmap f opt = case opt of 
    NoParam -> NoParam
    Param a -> Param (f a)

instance Applicative Optional where
  pure = Param 
  (<*>)  NoParam _ = NoParam
  (<*>)  (Param a) s = fmap a s


